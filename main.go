package main

import (
	"gitlab.com/osaconfig/inventory-cli/cmd"
)

func main() {
	cmd.Execute()
}
