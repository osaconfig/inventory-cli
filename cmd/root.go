package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var listFlag bool
var hostFlag string

// RootCmd is the starting cobra command, this will proccess the --list and --host flags
var RootCmd = &cobra.Command{
	Use:   "inventory-cli",
	Short: "OSAConfig CLI for the Inventory service",
	Long:  `Acts as a CLI for the OSAConfig inventory-service as well as a dynamic inventory plugin replacement for OSA.`,
	Run: func(cmd *cobra.Command, args []string) {
		if listFlag {
			List()
		}
		if hostFlag != "" {
			Host(hostFlag)
		}
	},
}

func init() {
	RootCmd.AddCommand(listCmd)
	RootCmd.Flags().BoolVarP(&listFlag, "list", "l", false, "list inventory")
	RootCmd.Flags().StringVarP(&hostFlag, "host", "H", "", "query host from inventory")
}

// Execute is the entrypoint
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
