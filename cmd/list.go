package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/Jeffail/gabs"
	"github.com/spf13/cobra"
)

const (
	// DEBUG Do we want debugging
	DEBUG = false
)

// IntermediateInv - struct for storing our intermediate JSON
type IntermediateInv struct {
	Inventory map[string]interface{}
}

// OSAConfig - Top level struct to contain sections of inventory
type OSAConfig struct {
	Meta HostVars  `json:"_meta"`
	All  GroupVars `json:"all"`
}

// HostVars I have no idea what I'm doing
type HostVars struct {
	Hostvars map[string]M `json:"hostvars"`
}

// GroupVars things
type GroupVars struct {
	Groupvars M `json:"vars"`
}

// M generic string/interface map
type M map[string]interface{}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List entire inventory",
	Long:  `List the entire inventory in Ansible consumable JSON.`,
	Run: func(cmd *cobra.Command, args []string) {
		List()
	},
}

// List returns the entire inventory as a single JSON object.  This is a requirement for an Ansible dynamic-inventory
func List() {
	var inter IntermediateInv
	var osa OSAConfig
	var hv HostVars
	var groups map[string]M
	// var hvbytes []byte

	inventory, err := fetchInventory()
	err = json.Unmarshal(inventory, &inter.Inventory)
	if err != nil {
		log.Panicf("Intermediate unmarshal failure: %v", err)
	}

	for k, v := range inter.Inventory["inventory"].(map[string]interface{}) {
		switch k {
		case "environment":
		case "host_vars":
			var osaHosts []interface{}
			osaHosts = v.(map[string]interface{})["osaHosts"].([]interface{})
			hv.Hostvars = make(map[string]M)
			for i := 0; i < len(osaHosts); i++ {
				x := osaHosts[i].(map[string]interface{})
				name := x["osa_inventory_name"].(string)
				if hv.Hostvars[name] == nil {
					hv.Hostvars[name] = make(M)
				}
				hv.Hostvars[name] = x
			}
		case "group_vars":
			var gv GroupVars
			gv.Groupvars = v.(map[string]interface{})
			osa.All = gv
		default:
			cg := v.([]interface{})
			groups = make(map[string]M)
			for i := 0; i < len(cg); i++ {
				x := cg[i].(map[string]interface{})
				name := x["name"].(string)
				if groups[name] == nil {
					groups[name] = make(M)
				}
				groups[name] = x
			}
		}
	}
	osa.Meta = hv
	b, err := json.MarshalIndent(osa, "", "  ")
	if err != nil {
		log.Panic(err)
	}
	c, err := json.MarshalIndent(groups, "", "  ")
	if err != nil {
		log.Panic(err)
	}
	meta, _ := gabs.ParseJSON(b)
	gv, _ := gabs.ParseJSON(c)
	meta.Merge(gv)

	//os.Stdout.Write(meta.String())
	fmt.Print(meta.String())

	if DEBUG {
		b, err := json.MarshalIndent(inter, "", "  ")
		if err != nil {
			log.Println("error: ", err)
		}
		os.Stdout.Write(b)
	}
}
