package cmd

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func fetchInventory() ([]byte, error) {
	//TODO(d34dh0r53): Move this to viper
	url := "http://micro.osaconfig.d34dh0r53.com/rpc"

	invClient := &http.Client{
		Timeout: time.Second * 2, // Max of 2 seconds
	}

	//TODO(d34dh0r53): Move environment to viper
	requestBody := []byte(`
		{
			"request": {
				"environment": "testing-1"
			},
			"method": "InventoryService.GetInventory",
			"service": "inventory-service"
		}`)

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBody))
	req.Header.Set("Content-Type", "application/json")

	resp, err := invClient.Do(req)
	if err != nil {
		log.Println("client errror: ", err)
		return nil, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	return body, nil
}
