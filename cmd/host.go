package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var hostCmd = &cobra.Command{
	Use:   "host",
	Short: "Pull specific host from inventory",
	Long:  `Search the inventory and return the requested host in Ansible consumable JSON.`,
	Run: func(cmd *cobra.Command, args []string) {
		Host(args[0])
	},
}

// Host dumps the ansible consumable JSON for the host
func Host(hostname string) error {
	log.Println("Hostname: ", hostname)

	return nil
}
