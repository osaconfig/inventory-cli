POD=$(shell kubectl -n osaconfig  get pods -l app=userconfig-service -o jsonpath="{.items[0].metadata.name}")
INVENTORY_SERVICE_IP=$(shell kubectl -n osaconfig get services -l app=inventory-service -o jsonpath="{.items[0].spec.clusterIP}")
setup:
	sudo mkdir -p /var/run/secrets/kubernetes.io/serviceaccount
	sudo chgrp wheel /var/run/secrets/kubernetes.io/serviceaccount
	sudo chmod g+w /var/run/secrets/kubernetes.io/serviceaccount
	kubectl -n osaconfig -it exec $(POD) cat /var/run/secrets/kubernetes.io/serviceaccount/ca.crt > /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
	kubectl -n osaconfig -it exec $(POD) cat /var/run/secrets/kubernetes.io/serviceaccount/token > /var/run/secrets/kubernetes.io/serviceaccount/token
	kubectl -n osaconfig -it exec $(POD) cat /var/run/secrets/kubernetes.io/serviceaccount/namespace > /var/run/secrets/kubernetes.io/serviceaccount/namespace
	@echo "To set the environment variable for the inventory-inventory service endpoint you can run:"
	@echo "export INVENTORY_SERVICE_IP=$(INVENTORY_SERVICE_IP)"
